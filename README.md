# Olá, sou Elton Sousa!

<img src="https://gitlab.com/eltonsousa/portifolio/-/raw/master/public/_img/foto_perfil.png" alt="foto perfil" width=200px >
<!-- <img src="https://i.imgur.com/JBmtEzz.png" alt="foto perfil" width=200px > -->
<!-- ![Foto perfil](https://i.imgur.com/JBmtEzz.png) -->

## Designer e desenvolvedor Front-end.

---

## Sobre

### Um pouco sobre mim

Olá, me chamo Elton Sousa, tenho 37 anos e estou cursando o 6º período de Design Gráfico no Centro de Ensino do Norte - UNINORTE-MANAUS, sou apaixonado por tecnologia. Aprendiz e entusiasta em Front-end, ultimamente estou fazendo cursos nas plataformas Origamid e Udemy, também no Youtube e nos principais sites relacionados.

---

### Cursos e especializações:

- Design Gráfico
- Web Design
- UI Design

---

### Desenvolvimento Front-end:

- HTML5
- CSS

---

### Ferramentas que utilizo:

- Corel Draw
- Illustrator
- Photoshop
- Adobe XD
- Figma

---

### Noções básicas em::

- Servidores Linux
- Shell script
- PHP

---

## Projetos

### [Maqform](https://my-pages-front-end.gitlab.io/projetos-origamid/maqform/)

Design criado no Adobe XD. Site desenvolvido em HTML5, CSS e um pouquinho de JS.

### [Bikcraft](https://my-pages-front-end.gitlab.io/projetos-origamid/bikcraft/)

Wireframe criado no Adobe XD. projeto de estudos do curso de Web design da Origamid.

### [Strata](https://my-pages-front-end.gitlab.io/projetos-udemy/projeto-strata/)

Projeto de estudo do curso de Webdesign da Udemy.

---

## Contato

- _[Telegram](https://t.me/eltonsousa)_
- _[Whatsapp](https://api.whatsapp.com/send?phone=5592993312208")_
- _eltonsousadesigner@gmail.com_
